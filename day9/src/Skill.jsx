import React, { Component } from 'react'
import { Col, Grid, Row } from "react-bootstrap"

class Skill extends Component {
    render() {
        return (
            <Grid>
                <div className="container">
                    <Row>
                        <h1>My Skill</h1>
                    </Row>
                    <Row>
                        <h3>1: React JS</h3>
                    </Row>
                    <Row>
                        <h3>2: Meteor JS</h3>
                    </Row>
                    <Row>
                        <h3>3: MongoDB</h3>
                    </Row>
                </div>
            </Grid>
        )
    }
}

export default Skill