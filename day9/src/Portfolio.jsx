import React, { Component } from 'react'
import { Col, Grid, Row } from 'react-bootstrap'

class Portfolio extends Component {
    render() {
        return (
            <Grid>
                <div className="container">
                    <Row>
                        <h1>My Portfolio</h1>
                    </Row>
                    <Row>
                        <h3>1: AS/RS WMS</h3>
                    </Row>
                    <Row>
                        <h3>2: SmartFactory</h3>
                    </Row>
                </div>
            </Grid>
        )
    }
}

export default Portfolio