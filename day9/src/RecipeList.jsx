import React, { Component } from 'react';
import { Col, Thumbnail } from "react-bootstrap";
import PropTypes from 'prop-types';

class RecipeList extends Component {
    render() {
        return (
            <div>
                <div className="box__img">
                    <Thumbnail alt="171x180" src={this.props.img_url} />
                </div>
                <div className="box__name">
                    <p>{this.props.name}</p>
                </div>
                <div className="box__ingredients">
                    <h6>ingredients :</h6>
                    <div className="box__ingredients__list">
                        {this.props.ingredients.map((rec, index) => {
                            return <p key={rec}>๐ {rec}</p>
                        })}
                    </div>
                </div>
                <div className="box__instructions">
                    <h6>instructions:</h6>
                    <p>{this.props.instructions}</p>
                </div>
            </div>
        )
    }
}

export default RecipeList;