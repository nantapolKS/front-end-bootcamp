import React, { Component } from 'react'
import { Switch, Route, NavLink } from 'react-router-dom';

import Home from './Home.jsx'
import About from './About.jsx'
import Jobs from './Jobs.jsx'
import Portfolio from './Portfolio.jsx'
import Skill from './Skill.jsx'
import CardDeek from './CardDeek.jsx'
import Recipe from './Recipe.jsx'
import BitCoin from './Bitcoin.jsx'

class MasterLayout extends Component {
    render() {
        return (
            <div className={`warpper`}>
                <div className={`sidebar`}>
                    <div className="sidebar__warpper">
                        <div className="navbar__link">
                            <ul>
                                <li><NavLink to="/" className="nav-link">home</NavLink></li>
                                <li><NavLink to="/about" className="nav-link">about</NavLink></li>
                                <li><NavLink to="/jobs" className="nav-link">jobs</NavLink></li>
                                <li><NavLink to="/portfolio" className="nav-link">Portfolio</NavLink></li>
                                <li><NavLink to="/skill" className="nav-link">Skill</NavLink></li>
                                <li><NavLink to="/day7_1" className="nav-link">day7_1 CardDeek</NavLink></li>
                                <li><NavLink to="/day7_2" className="nav-link">day7_2 Recipe</NavLink></li>
                                <li><NavLink to="/day8_1-2" className="nav-link">day8_1-2 BitCoin</NavLink></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="main">
                    <div className={`container`}>
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/about" component={About} />
                            <Route path="/jobs" component={Jobs} />
                            <Route path="/portfolio" component={Portfolio} />
                            <Route path="/skill" component={Skill} />
                            <Route path="/day7_1" component={CardDeek} />
                            <Route path="/day7_2" component={Recipe} />
                            <Route path="/day8_1-2" component={BitCoin} />
                        </Switch>
                    </div>
                </div>
            </div>
        );
    }

}

export default MasterLayout