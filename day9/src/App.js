import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

import MasterLayout from './MasterLayout.jsx'

const App = props => (
  <Router>
    <Switch>
      <Route exact path="/" component={MasterLayout} />
      <Route exact path="/about" component={MasterLayout} />
      <Route exact path="/jobs" component={MasterLayout} />
      <Route exact path="/portfolio" component={MasterLayout} />
      <Route exact path="/skill" component={MasterLayout} />
      <Route exact path="/day7_1" component={MasterLayout} />
      <Route exact path="/day7_2" component={MasterLayout} />
      <Route exact path="/day8_1-2" component={MasterLayout} />
    </Switch>
  </Router>
)

export default App
