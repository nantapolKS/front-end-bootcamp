import React, { Component } from 'react';
import { Col, Grid, Row, Button } from "react-bootstrap";

export default class CardDeek extends Component {

    constructor(props) {
        super(props);

        this.color = ['red', 'blue', 'green', 'purple', 'pink'];
        this.color.map(props => this.color = [...this.color, props])
        this.state = { color: this.color }

    }

    shuffle() {
        let color = this.state.color
        color.map((rec, index) => {
            let tmp = color[index], r = parseInt(Math.floor(Math.random() * color.length - index) + index);
            color[index] = color[r];
            color[r] = tmp;
        })
        this.setState({ color: color });
    }

    handleGetColor = value => e => {
        alert(value);
    }

    render() {
        return (
            <Grid>
                <Row>
                    {this.state.color.map((rec, index) => {
                        return (
                            <Col md={3} key={rec + index}>
                                <div className={`box__color ${rec}`} onClick={this.handleGetColor(rec)}></div>
                            </Col>
                        )
                    })}
                </Row>
                <Row>
                    <div className="center">
                        <Button bsStyle="primary" onClick={this.shuffle.bind(this)}>Shuffle</Button>
                    </div>
                </Row>
            </Grid>
        );
    }
};