import React, { Component } from 'react';
import { Col, Grid, Row, Button } from "react-bootstrap";
import Modal from 'react-modal';

import RecipeList from './RecipeList.jsx';

class Recipe extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: false,
            check: false,
            foods: [
                {
                    id: 0,
                    img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1uam-cq9JIr0bkwgfacR5WZ4M2BNkLAtPrtetf5tl5b9j_X8d",
                    name: "Spaghetti",
                    ingredients: ["pasta", "8 cups water", "1 box spaghetti"],
                    instructions: "Open jar of Spaghetti sauce. Bring to simmer. Boil water. Cook pasta unti done. Comboine pasta and sauce"
                },
                {
                    id: 1,
                    img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDKt6ZaAiMlaXvqS6E5I6JrOsr9Yy_2kIryxYG9SnA2XyQIngm",
                    name: "Milkeshake",
                    ingredients: ["2 Scoops Ice cream", "8 ounces milk"],
                    instructions: "Combine icecream and milk. Blend until creamy"
                },
                {
                    id: 2,
                    img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScDrNnS5_752e7VqoJ1-zxr07LaBh7_JoeWzWreOCbTcf0HxDb",
                    name: "Avocado Toast",
                    ingredients: ["2 slices of bread", "1 avocado", "1 tablespoon olive oil", "1 pinch of salt", "pepper"],
                    instructions: "Toast bread. Slice avocado and spread on bread. Add salt, oil, and pepper to taste."
                },
            ],
            searchfoods: [],
            input_ingredients: [""]
        }
    }

    componentWillMount() {
        Modal.setAppElement('body');
    }

    handleDeletefoodsList = value => e => {
        let foods = this.state.foods;
        foods = foods.filter(element => { return element.id !== value });
        this.setState({ foods: foods })
    }

    onChange(e) {
        e.preventDefault();
        let search = this.refs.search.value.toLowerCase();
        if (search) this.setState({ check: true })
        else this.setState({ check: false })
        let foodslist = this.state.foods;
        let data = foodslist.filter(val => {
            let name = val.name.toLowerCase();
            let test = name.indexOf(search);
            if (test !== -1) {
                return val
            }
        })
        this.setState({ searchfoods: data })
    }

    Ingredients = value => e => {
        let data = this.state.input_ingredients;
        data[value] = e.target.value;
        this.setState({ input_ingredients: data });
    }

    onSubmit(e) {
        e.preventDefault();

        let img_url = this.refs.txt_img_url.value.trim();
        let name = this.refs.txt_name.value.trim();
        let ingredients = this.state.input_ingredients;
        let instructions = this.refs.txt_instructions.value.trim();

        const foods = {
            id: this.state.foods.length,
            img_url: img_url,
            name: name,
            ingredients: ingredients,
            instructions: instructions
        }

        this.setState({
            foods: [...this.state.foods, foods],
            input_ingredients: [""],
            isOpen: false
        })

    }

    handleModalClose() {
        this.setState({
            input_ingredients: [""],
            isOpen: false
        });
    }

    RecipeList() {
        if (!this.state.check) {
            return (
                <Row>
                    {this.state.foods.map((record, index) => {
                        return (
                            <Col key={record.id} sm={12} md={6} xs={4} lg={4}>
                                <div className="box__body">
                                    <RecipeList
                                        img_url={record.img_url}
                                        name={record.name}
                                        ingredients={record.ingredients}
                                        instructions={record.instructions}
                                    />
                                    <Row>
                                        <div className="center">
                                            <Button bsStyle="primary" onClick={this.handleDeletefoodsList(record.id)}>Delete</Button>
                                        </div>
                                    </Row>
                                </div>
                            </Col>
                        )
                    })}
                </Row>
            )
        } else {
            return (
                <Row>
                    {this.state.searchfoods.map((record, index) => {
                        return (
                            <Col key={record.id} sm={12} md={6} xs={4} lg={4}>
                                <div className="box__body">
                                    <RecipeList
                                        img_url={record.img_url}
                                        name={record.name}
                                        ingredients={record.ingredients}
                                        instructions={record.instructions}
                                    />
                                    <Row>
                                        <div className="center">
                                            <Button bsStyle="primary" onClick={this.handleDeletefoodsList(record.id)}>Delete</Button>
                                        </div>
                                    </Row>
                                </div>
                            </Col>
                        )
                    })}
                </Row>
            )
        }
    }

    render() {
        return (
            <Grid>
                <br />
                <Row>
                    <Col sm={6}>
                        <input type="text" name="search" id="search" ref="search" onChange={this.onChange.bind(this)} />
                    </Col>
                    <Col sm={6}>
                        <Button bsStyle="primary" onClick={() => this.setState({ isOpen: true })}>Add Recipe</Button>
                    </Col>
                </Row>
                {this.RecipeList()}
                <Modal
                    isOpen={this.state.isOpen}
                    contentLabel="Add Recipe"
                    onAfterOpen={() => this.refs.txt_name.focus()}
                    onRequestClose={this.handleModalClose.bind(this)}
                    className="boxed-view__box"
                    overlayClassName="boxed-view boxed-view--modal">
                    <h1>Add Recipek</h1>
                    <form onSubmit={this.onSubmit.bind(this)} className="boxed-view__form">
                        <input
                            type="text"
                            placeholder="Name"
                            ref="txt_name"
                        />
                        {this.state.input_ingredients.map((_, index) => {
                            return (
                                <input
                                    key={index}
                                    type="text"
                                    placeholder={`Ingredients${index + 1}`}
                                    name={`txt_ingredients${index + 1}`}
                                    onChange={this.Ingredients(index)}
                                />
                            )
                        })}
                        <a className="button button--pill" onClick={() => {
                            let txt_ingredients = { txt_ingredients: "" }
                            this.setState({ input_ingredients: [...this.state.input_ingredients, txt_ingredients] })
                        }}
                        >Add Ingredients</a>
                        <input
                            type="text"
                            placeholder="Instructions"
                            ref="txt_instructions"
                        />
                        <input
                            type="text"
                            placeholder="URL"
                            ref="txt_img_url"
                        />
                        <button className="button">Add</button>
                        <button type="button" className="button button--secondary"
                            onClick={this.handleModalClose.bind(this)}>Cancel
                        </button>
                    </form>
                </Modal>
            </Grid >
        )
    }
}

export default Recipe;