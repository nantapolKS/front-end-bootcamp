export default class Employee {
    constructor(firstname, lastname, salary) {
        this._firstname = firstname;
        this._lastname = lastname;
        this._salary = salary; // simulate private variable
    }
    setSalary(newSalary) { // simulate public method
        this._salary < newSalary ? (
            this._salary = newSalary,
            console.log(`${this._firstname}'s salary has been set to ${this._salary}`)
        ) : (
            console.log(`${this._firstname}'s salary is less than before!!`)
        )
    }
    getSalary() { // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}