// let fetch = require('node-fetch')

// async function fetchAsync() {
//     // await response of fetch call
//     let response = await fetch('homework1');
//     // only proceed once promise is resolved
//     let data = await response.json();
//     // only proceed once second promise is resolved
//     return data;
// }

// trigger async function
// log response or catch error of fetch promise
// fetchAsync()
//     .then(data => console.log(data))
//     .catch(reason => console.log(reason.message))

// new Object
// let peopleLowSalary = [];
// peopleSalary.map((record, index) => {
//     if (record.salary < 100000) {
//         const data = {
//             id: record.id,
//             firstname: record.firstname,
//             lastname: record.lastname,
//             salary: record.salary * 2
//         }
//         return peopleLowSalary = [...peopleLowSalary, data]
//     }
// })

const peopleSalary = require('./homework1.json')

try {
    let peopleLowSalary = peopleSalary.filter(a => {
        return a.salary < 100000
    })

    peopleLowSalary.map(record => record.salary = record.salary * 2)

    // console.log(peopleSalary);

    console.log(peopleLowSalary);

    const sumSalary = peopleSalary.reduce((sum, record) => {
        return sum + record.salary
    }, 0)

    console.log(`sumSalary = ${sumSalary}`)
} catch (_) {
    console.log("EIEI");
}