const homework3 = require('./homework3.json')

let arr = [];

homework3
    .filter(record => {
        return record.friends.length >= 2 && record.gender === "male"
    })
    .map(record => {
        let balance = parseFloat(record.balance.slice(1).replace(/,/g, ""));
        const data = {
            name: record.name,
            gender: record.gender,
            company: record.company,
            email: record.email,
            friends: record.friends,
            balance: `$${balance /10}`
        }
        return arr = [...arr, data];
    })

console.log(arr);