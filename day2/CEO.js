import Employee from './Employee.js';

export default class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
    }
    getSalary() { // simulate public method
        return super.getSalary() * 2;
    };
    work(employee) { // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        employee.setSalary(newSalary)
    }
    gossip(employee, text = 'not anythink') {
        console.log(`Hey ${employee._firstname}, ${text}`)
    }

    _fire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(`${employee._firstname}  has been fired! Dress with :${this.dressCode}`);
    };

    _hire(employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(`${employee._firstname}  has been back! Dress with :${this.dressCode}`);
    };

    _seminar() { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar" + " Dress with :" + this.dressCode);
    };

    _golf() { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    };
}