$(() => {
    $("#txt_firstname").blur((event) => {
        let data = event.target.value
        data ? (
            $("#txt_firstname").addClass("is-valid"),
            $("#txt_firstname").removeClass("is-invalid")
        ) : (
            $("#txt_firstname").addClass("is-invalid"),
            $("#txt_firstname").removeClass("is-valid")
        )
    })

    $("#txt_lastname").blur((event) => {
        let data = event.target.value
        data ? (
            $("#txt_lastname").addClass("is-valid"),
            $("#txt_lastname").removeClass("is-invalid")
        ) : (
            $("#txt_lastname").addClass("is-invalid"),
            $("#txt_lastname").removeClass("is-valid")
        )
    })

    $("#txt_email").blur((event) => {
        function validateEmail(email) {
            let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test(email);
        }
        let data = event.target.value
        if (data) {
            if (!validateEmail(data)) {
                $("#text-email").html("กรุณากรอกเมลในรูปแบบ test@email.com");
                $("#txt_email").addClass("is-invalid")
                $("#txt_email").removeClass("is-valid")
            } else {
                $("#txt_email").addClass("is-valid")
                $("#txt_email").removeClass("is-invalid")
            }
        } else {
            $("#text-email").html("กรุณากรอกเมล")
            $("#txt_email").addClass("is-invalid")
            $("#txt_email").removeClass("is-valid")
        }
    })

    $("#txt_password").blur((event) => {

        function CheckPasswordStrength(password) {
            var password_strength = document.getElementById("password_strength");

            //TextBox left blank.
            if (password.length == 0) {
                password_strength.innerHTML = "";
                return;
            }

            //Regular Expressions.
            var regex = new Array();
            regex.push("[A-Z]"); //Uppercase Alphabet.
            regex.push("[a-z]"); //Lowercase Alphabet.
            regex.push("[0-9]"); //Digit.
            regex.push("[$@$!%*#?&]"); //Special Character.

            var passed = 0;

            //Validate for each Regular Expression.
            for (var i = 0; i < regex.length; i++) {
                if (new RegExp(regex[i]).test(password)) {
                    passed++;
                }
            }

            //Validate for length of Password.
            if (passed > 2 && password.length > 8) {
                passed++;
            }

            //Display status.
            var color = "";
            var strength = "";
            switch (passed) {
                case 0:
                case 1:
                    strength = "Weak";
                    color = "red";
                    break;
                case 2:
                    strength = "Good";
                    color = "darkorange";
                    break;
                case 3:
                case 4:
                    strength = "Strong";
                    color = "green";
                    break;
                case 5:
                    strength = "Very Strong";
                    color = "darkgreen";
                    break;
            }
            password_strength.innerHTML = strength;
            password_strength.style.color = color;
        }

        let data = event.target.value
        data ? (
            CheckPasswordStrength(data),
            $("#txt_password").addClass("is-valid"),
            $("#txt_password").removeClass("is-invalid")
        ) : (
            $("#txt_password").addClass("is-invalid"),
            $("#txt_password").removeClass("is-valid")
        )
    })

    $("#btnOK").click(() => {
        let num = $("#number").val()
        if (num) {
            console.log(num);
            for (i = 1; i <= 12; i++) console.log(`${num} X ${i} = ${num*i}`);
        }

        fetch('https://swapi.co/api/people')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                res.results.map((record, index) => {
                    let txt1 = `<p>${record.name}</p>`
                    $("#result").append(txt1);
                })
            });
    })
})