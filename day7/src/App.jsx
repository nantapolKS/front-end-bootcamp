import React, { Component } from 'react';
import { Grid } from "react-bootstrap";

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import CardDeek from './CardDeek.jsx';
import Recipe from './Recipe.jsx';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      color: ['red', 'blue', 'green', 'purple', 'pink']
    }
  }

  render() {
    return (
      <div className="warpper">
        <div className="header">
          <div className="header__link">
            <a href="#">Recipe App</a>
          </div>
          <div className="header__menu">
            <a href="#">New Recipe</a>
            <a href="#">Home</a>
            <a href="#">Abort</a>
            <a href="#">Contect</a>
          </div>
        </div>
        {/* <CardDeek color={this.state.color} /> */}
        <Recipe />
        {/* <div className="footer">
          <p>this is footer</p>
        </div> */}
      </div>
    );
  }
};

export default App;
