import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import React, { Component } from 'react';
import axios from 'axios';
import { Row, Col } from 'antd';
import { Select } from 'antd';
import { Spin } from 'antd';

const Option = Select.Option;

class Bitcoin extends Component {

    constructor(props) {
        super(props);

        this.state = {
            bitcoin: undefined,
            bpilist: [],
            val: [],
            selected: "USD",
            data: []
        }
    }

    componentDidMount() {
        this.timer = setInterval(() => this.getBitcoin(), 5000)
        this.getCoindesk()
    }

    async getBitcoin() {
        try {
            const response = await axios.get('https://api.coindesk.com/v1/bpi/currentprice.json');
            this.setState({
                bitcoin: response.data,
                bpilist: Object.keys(response.data.bpi),
            })
        } catch (error) {
            console.error(error);
        }
    }

    async getCoindesk(currency = 'USD') {
        try {
            const response = await axios.get(`https://api.coindesk.com/v1/bpi/historical/close.json?currency=${currency}`);
            let count = Object.keys(response.data.bpi).length;
            let data = []
            for (let i = 0; i < count; i++)
                data = [...data, { name: Object.keys(response.data.bpi)[i], value: Object.values(response.data.bpi)[i] }]
            this.setState({ data: data })

        } catch (error) {
            console.error(error);
        }
    }

    handleChange(e) {
        this.setState({ selected: e })
        this.getCoindesk(e);
    }


    render() {
        if (this.state.bitcoin) {
            return (
                <Row>
                    <Col span={12}>
                        <Select defaultValue="USD" style={{ width: 240 }} onChange={this.handleChange.bind(this)}>
                            {this.state.bpilist.map(rec => { return <Option key={rec} value={rec}>{rec}</Option> })}
                        </Select>
                    </Col>
                    <Col span={12}>
                        <h2>Current price: {this.state.bitcoin ? `${this.state.bitcoin.bpi[this.state.selected].rate_float} ${this.state.bitcoin.bpi[this.state.selected].code}` : ``}</h2>
                    </Col>
                    <Col span={24}>
                        {
                            this.state.data ? (
                                <LineChart width={800} height={300} data={this.state.data}
                                    margin={{ top: 5, right: 30, left: 20, bottom: 5 }}>
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Line type="monotone" dataKey="value" name={this.state.selected} stroke="#8884d8" />
                                </LineChart>
                            ) : (
                                    <div className="loader">
                                        <Spin size="large" />
                                    </div>
                                )
                        }
                    </Col>
                </Row>
            )
        } else {
            return (
                <div className="loader">
                    <Spin size="large" />
                </div>
            )
        }
    }
}

export default Bitcoin;
