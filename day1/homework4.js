var http = require('http');
http.createServer(function (req, res) {
    res.writeHead(200, {
        'Content-Type': 'text/plain'
    });
    res.end('Hello World\n');
}).listen(1337, "127.0.0.1");

let people = require('./homework4.json');

let eyeColor = {
    brown: 0,
    green: 0,
    blue: 0
};

let gender = {
    male: 0,
    female: 0
}

people.map((record, index) => {
    if (record.eyeColor === 'brown')
        eyeColor.brown++;
    else if (record.eyeColor === 'green')
        eyeColor.green++;
    else if (record.eyeColor === 'blue')
        eyeColor.blue++;
    if (record.gender === 'male')
        gender.male++;
    else if (record.gender === 'female')
        gender.female++;

    let friendCount = 0;

    record.friends.map((rec) => friendCount++)

    people[index].friendCount = friendCount;
});

let people_count = {
    eyeColor: eyeColor,
    gender: gender,
}