import React, { Component } from 'react';

class RandomBox extends Component {

    constructor(props) {
        super(props);
        this.fontrandom = (parseInt(Math.random() * 40) + 20);
        this.fontstyle = {
            fontSize: `${this.fontrandom}px`
        }
        this.color_box = ['red', 'blue', 'green', 'purple', 'pink']
    }

    render() {
        return (
            <div className={`box__body ${this.color_box[(parseInt(Math.random() * this.color_box.length) + 0)]}`}>
                <p style={this.fontstyle}>Random Box</p>
            </div>
        );
    }
};

export default RandomBox;