import React, { Component } from 'react';
import { Col, Grid, Row } from "react-bootstrap";

class Card extends Component {

    constructor(props) {
        super(props);

        this.foods = [
            {
                img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1uam-cq9JIr0bkwgfacR5WZ4M2BNkLAtPrtetf5tl5b9j_X8d",
                name: "Spaghetti",
                ingredients: ["pasta", "8 cups water", "1 box spaghetti"],
                instructions: "Open jar of Spaghetti sauce. Bring to simmer. Boil water. Cook pasta unti done. Comboine pasta and sauce"
            },
            {
                img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSDKt6ZaAiMlaXvqS6E5I6JrOsr9Yy_2kIryxYG9SnA2XyQIngm",
                name: "Milkeshake",
                ingredients: ["2 Scoops Ice cream", "8 ounces milk"],
                instructions: "Combine icecream and milk. Blend until creamy"
            },
            {
                img_url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcScDrNnS5_752e7VqoJ1-zxr07LaBh7_JoeWzWreOCbTcf0HxDb",
                name: "Avocado Toast",
                ingredients: ["2 slices of bread", "1 avocado", "1 tablespoon olive oil", "1 pinch of salt", "pepper"],
                instructions: "Toast bread. Slice avocado and spread on bread. Add salt, oil, and pepper to taste."
            },
        ]
    }

    render() {
        return (
            <Row>
                {this.foods.map((record => {
                    return (
                        <Col key={record.name} sm={12} md={6} xs={4} lg={4}>
                            <div className="box__body">
                                <div>
                                    <img src={record.img_url} alt="" />
                                </div>
                                <div className="box__name">
                                    <p>{record.name}</p>
                                </div>
                                <div className="box__ingredients">
                                    <h6>ingredients :</h6>
                                    <div className="box__ingredients__list">
                                        {
                                            record.ingredients.map((rec, index) => {
                                                return <p key={record.ingredients[index]}>๐ {record.ingredients[index]}</p>
                                            })
                                        }
                                    </div>
                                </div>
                                <div className="box__instructions">
                                    <h6>instructions:</h6>
                                    <p>{record.instructions}</p>
                                </div>
                            </div>
                        </Col>
                    )
                }))
                }
            </Row>
        )
    }
}

export default Card;